scalaVersion := "3.1.0"

libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "42.3.1",
  "org.tpolecat" %% "skunk-core" % "0.2.2",
  "org.typelevel" %% "cats-core" % "2.7.0",
  "org.typelevel" %% "cats-effect" % "3.3.4",
  "org.typelevel" %% "mouse" % "1.0.8",
  "org.scalameta" %% "munit" % "0.7.27" % Test,
  "org.scalacheck" %% "scalacheck" % "1.15.4" % Test
)